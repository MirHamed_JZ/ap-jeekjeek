#include <iostream>
#include "Jeek.hpp"

using namespace std;

Jeek::Jeek()
{
	string commandLine;
	while(getline(cin,commandLine))
	{
		string commandLineOption = findWhiteSpaces(commandLine);
		commandLine.erase(0,commandLineOption.size());
		try {
			bool publishJeek = doOperation(commandLine, commandLineOption);
			if (publishJeek == true)
				break;
		}
		catch(BadOperationException badException) {
			cout << COMMAND_FAILED << endl;
		}
		catch(TextSizeException badException) {
			cout << TEXT_SIZE_OVERFLOW << endl;
		}
		catch(JeekTextException badException) {
			cout << JEEK_TEXT_EMPTY << endl;
		}
	}
	rejeeksNumber = 0;
}

Jeek::Jeek(std::string _jeekText)
{
    jeekText = _jeekText;
    rejeeksNumber = 0;
}

bool Jeek::doOperation(string commandLine, string commandLineOption)
{
	if (commandLineOption == TEXT)
		text(commandLine);
	else if (commandLineOption == TAG)
		tag(commandLine);
	else if (commandLineOption == MENTION)
		mention(commandLine);
	else if (commandLineOption == ABORT)
	{
		if (findWhiteSpaces(commandLine) != NOTHING)
			throw BadOperationException();
		throw AbortedJeek();
	}
	else if (commandLineOption == PUBLISH)
	{
		if (findWhiteSpaces(commandLine) != NOTHING)
			throw BadOperationException();
		else if (jeekText == NOTHING)
			throw JeekTextException();
		return true;
	}
	else
		throw BadOperationException();
	return false;
}

void Jeek::setJeekBaseInfo(string _jeekId, string _jeekerDisplayName, string _jeekerUsername)
{
	jeekId = _jeekId;
	jeekerDisplayName = _jeekerDisplayName;
	jeekerUsername = _jeekerUsername;
}

void Jeek::text(string commandLine)
{
	findWhiteSpaces(commandLine);
	string textString = commandLine;

	if (textString.size() > TEXT_SIZE)
		throw TextSizeException();

	jeekText = textString;
}

void Jeek::tag(string commandLine)
{
	string hashtagString = findWhiteSpaces(commandLine);
	commandLine.erase(0,hashtagString.size());

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();

	jeekHashtags.push_back(hashtagString);
}

void Jeek::mention(string commandLine)
{
	string mentionedUsername = findWhiteSpaces(commandLine);
	commandLine.erase(0,mentionedUsername.size());

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();

	jeekMentions.push_back(mentionedUsername);
}

Comment* Jeek::addComment(string commentText, string displayName, string username)
{
	jeekComments.push_back(new Comment(commentText, displayName));
	jeekComments.back()->setCommentBaseInfo(generateCommentId(), username);
	return jeekComments.back();
}

string Jeek::generateCommentId()
{
	string newId = jeekId + COMMENT_ID;
	srand(time(NULL));
	int newIdNum = rand();
	newId.append(intToString(newIdNum));
	return newId;
}

Reply* Jeek::addReplyForComment(string replyText, string commentId, string displayName, string username)
{
	int commentNum = findComment(commentId);

	if (commentNum == NOT_A_COMMENT)
    	throw CommentFindingException();

    return jeekComments[commentNum]->addReplyForComment(replyText, displayName, username);
}

Reply* Jeek::addReplyForReply(string replyText, string replyId, string displayName, string username)
{
	string commentId = divideString(replyId, 0, replyId.find(REPLY_ID));
	int commentNum = findComment(commentId);

	if (commentNum == NOT_A_COMMENT)
    	throw ReplyFindingException();

    return jeekComments[commentNum]->addReplyForReply(replyText, replyId, displayName, username);
}

Jeek* Jeek::rejeek()
{
	rejeeksNumber++;
	string newText = REJEEKED_TITLE + jeekText;
	Jeek* rejeekedJeek = new Jeek(newText);
	return rejeekedJeek;
}

void Jeek::showCompeleteJeek()
{
	cout << jeekerDisplayName << endl << jeekText << endl;
	for (int i = 0; i < jeekHashtags.size(); i++)
		cout << HASHTAG << jeekHashtags[i] << endl;
	for (int i = 0; i < jeekMentions.size(); i++)
		cout << ATSIGN << jeekMentions[i] << endl;
	cout << LIKES_TITLE << jeekLikers.size() << endl;
	cout << REJEEKS_TITLE << rejeeksNumber << endl;
	cout << COMMENTS_TITLE << endl;
	for (int i = 0; i < jeekComments.size(); i++)
		cout << jeekComments[i]->getCommentId() << endl;
}

bool Jeek::showJeekForHashtag(string hashtagString)
{
	if (isFoundHashtag(hashtagString) == true)
	{
		cout << jeekId << SPACE << jeekerDisplayName << endl;
		cout << jeekText << endl << endl;
		return true;
	}
	return false;
}

bool Jeek::isFoundHashtag(string hashtagString)
{
	for (int i = 0; i < jeekHashtags.size(); i++)
		if (jeekHashtags[i] == hashtagString)
			return true;
	return false;
}

void Jeek::showComment(string commentId)
{
	int commentNum = findComment(commentId);

	if (commentNum == NOT_A_COMMENT)
    	throw CommentFindingException();

    jeekComments[commentNum]->showComment();
}

void Jeek::showReply(string replyId)
{
	string commentId = divideString(replyId, 0, replyId.find(REPLY_ID));
	int commentNum = findComment(commentId);

	if (commentNum == NOT_A_COMMENT)
    	throw ReplyFindingException();

    jeekComments[commentNum]->showReply(replyId);
}

void Jeek::like(string username)
{
	if (isJeekLikedBefore(username) == true)
		throw JeekLikeException();

	jeekLikers.push_back(username);
}

void Jeek::dislike(string username)
{
	if (isJeekLikedBefore(username) == false)
		throw JeekDislikeException();

	deleteLikerFromList(username);
}

bool Jeek::isJeekLikedBefore(string username)
{
	for (int i = 0; i < jeekLikers.size(); i++)
		if (jeekLikers[i] == username)
			return true;
	return false;
}

void Jeek::deleteLikerFromList(string username)
{
	for (int i = 0; i < jeekLikers.size(); i++)
	{
		if (jeekLikers[i] == username)
		{
			for (int j = i; j < jeekLikers.size() - 1; j++)
				jeekLikers[j] = jeekLikers [j + 1];
			jeekLikers.pop_back();
			break;
		}
	}
}

int Jeek::findComment(string commentId)
{
	for (int i = 0; i < jeekComments.size(); i++)
		if (jeekComments[i]->getCommentId() == commentId)
			return i;
	return NOT_A_COMMENT;
}

string Jeek::findUserOfComment(string commentId)
{
	for (int i = 0; i < jeekComments.size(); i++)
		if (jeekComments[i]->getCommentId() == commentId)
			return jeekComments[i]->getCommenterUsername();
	return NOTHING;
}

string Jeek::findUserOfReply(string replyId)
{
	string commentId = divideString(replyId, 0, replyId.find(REPLY_ID));
	int commentNum = findComment(commentId);
	return jeekComments[commentNum]->findUserOfReply(replyId);
}

string divideString(string inputString, int beginingPos, int endingPos)
{
	string outputString = NOTHING;

	if (beginingPos < 0 || endingPos > inputString.size())
		return NOTHING;
		
	for (int i = beginingPos; i < endingPos; i++)
			outputString += inputString[i];
	return outputString;
}

string findPartOfLine(string completeLine)
{
	string partOfLine = NOTHING;
	for (int i = 0; i < completeLine.size(); i++)
	{
		if ((completeLine[i] != SPACE) && (completeLine[i] != TAB))
			partOfLine += completeLine[i];
		else
			return partOfLine;
	}
	return partOfLine;
}

string findWhiteSpaces(string& completeLine)
{
	string partOfLine = findPartOfLine(completeLine);
	if (partOfLine != NOTHING)
		return partOfLine;
	else
	{
		if (completeLine != NOTHING)
		{
			completeLine.erase(0,1);
			return findWhiteSpaces(completeLine);
		}
		else
			return partOfLine;
	}
}