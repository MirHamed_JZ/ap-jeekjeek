#ifndef __JEEK_H__
#define __JEEK_H__

#include "comment.hpp"

#define TAB '\t'
#define SPACE ' '
#define ATSIGN '@'
#define HASHTAG '#'
#define TEXT_SIZE 140
#define MENTION "mention"
#define PUBLISH "publish"
#define ABORT "abort"
#define TEXT "text"
#define TAG "tag"
#define JEEK_ID "_J_"
#define LIKES_TITLE "Likes "
#define REJEEKS_TITLE "Rejeeks "
#define REJEEKED_TITLE "Rejeeked:"
#define COMMAND_FAILED "Command is incorrect."
#define TEXT_SIZE_OVERFLOW "Text size must be less than or equal to 140 character."
#define JEEK_TEXT_EMPTY "Your Jeek's text is empty."
#define JEEK_ABORTED "Jeek is aborted."
#define JEEK_NOT_FOUND "There is no Jeek with this information."
#define JEEK_PUBLISHED "Jeek is published."
#define JEEK_REJEEKED "Jeek successfully rejeeked."
#define JEEK_LIKED "Liked successfully."
#define JEEK_DISLIKED "Disliked successfully."
#define LIKED_BEFORE "You liked this jeek before."
#define NOT_LIKED_BEFORE "You didn't like this jeek before."
#define NO_JEEK_FOR_HASHTAG "There is no Jeek with this hashtag."

class BadOperationException {};
class JeekFindingException {};
class TextSizeException {};
class JeekTextException {};
class JeekLikeException {};
class JeekDislikeException {};
class AbortedJeek {};

std::string divideString(std::string inputString, int beginingPos, int endingPos);
std::string findPartOfLine(std::string completeLine);
std::string findWhiteSpaces(std::string& completeLine);

class Jeek
{
  public:
    Jeek();
    Jeek(std::string _jeekText);
    Comment* addComment(std::string commentText, std::string displayName, std::string username);
    Reply* addReplyForComment(std::string replyText, std::string commentId, std::string displayName, std::string username);
    Reply* addReplyForReply(std::string replyText, std::string replyId, std::string displayName, std::string username);
    Jeek* rejeek();
    void showCompeleteJeek();
    bool showJeekForHashtag(std::string hashtagString);
    void showComment(std::string commentId);
    void showReply(std::string replyId);
    void like(std::string username);
    void dislike(std::string username);
    bool isJeekLikedBefore(std::string username);
    void deleteLikerFromList(std::string username);
    void setJeekBaseInfo(std::string _jeekId, std::string _jeekerDisplayName, std::string _jeekerUsername);
    std::string getJeekId() { return jeekId; }
    std::string getJeekerUsername() { return jeekerUsername; }
    std::string getJeekText() { return jeekText; }
    std::vector<std::string> getJeekMentions() { return jeekMentions; }
    std::string findUserOfComment(std::string commentId);
    std::string findUserOfReply(std::string replyId);
  private:
    std::string jeekerDisplayName;
    std::string jeekerUsername;
    std::string jeekId;
    std::string jeekText;
    std::vector<std::string> jeekHashtags;
    std::vector<std::string> jeekMentions;
    std::vector<Comment*> jeekComments;
    std::vector<std::string> jeekLikers;
    int rejeeksNumber;

    bool doOperation(std::string commandLine, std::string commandLineOption);
    void text(std:: string commandLine);
    void tag(std::string commandLine);
    void mention(std::string commandLine);
    bool isFoundHashtag(std::string hashtagString);
    std::string generateCommentId();
    int findComment(std::string commentId);
};

#endif