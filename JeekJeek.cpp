#include <iostream>
#include "JeekJeek.hpp"

using namespace std;

void JeekJeek::commandLineOperation(string commandLine)
{
	try
	{
		string commandLineOption = findWhiteSpaces(commandLine);
		commandLine.erase(0,commandLineOption.size());
		doOperation(commandLine, commandLineOption);
	}
	catch(BadOperationException badException)
	{
		cout << COMMAND_FAILED << endl;
	}
	catch(UserDuplicateException badException)
	{
		cout << USER_DUPLICATE << endl;
	}
	catch(UserLoginException1 badException)
	{
		cout << USER_IS_ONLINE << endl;
	}
	catch(UserLoginException2 badException)
	{
		cout << LOGIN_FAILED << endl;
	}
	catch(UserLogoutException badException)
	{
		cout << NO_USER_ONLINE << endl;
	}
	catch(JeekFindingException badException)
	{
		cout << JEEK_NOT_FOUND << endl;
	}
	catch(CommentFindingException badException)
	{
		cout << COMMENT_NOT_FOUND << endl;
	}
	catch(ReplyFindingException badException)
	{
		cout << REPLY_NOT_FOUND << endl;
	}
	catch(TextException badException)
	{
		cout << TEXT_EMPTY << endl;
	}
	catch(UserFindingException)
	{
		cout << USER_NOT_FOUND << endl;
	}
}

void JeekJeek::doOperation(string commandLine, string commandLineOption)
{
	if (commandLineOption == SIGNUP)
		signup(commandLine);
	else if (commandLineOption == LOGIN)
		login(commandLine);
	else if (commandLineOption == LOGOUT)
		logout(commandLine);
	else if (commandLineOption == SEARCH)
		search(commandLine);
	else if (commandLineOption == COMMENT)
		comment(commandLine);
	else if (commandLineOption == REPLY)
		reply(commandLine);
	else if (commandLineOption == REJEEK)
		rejeek(commandLine);
	else if (commandLineOption == SHOW_JEEK)
		showJeek(commandLine);
	else if (commandLineOption == SHOW_COMMENT)
		showComment(commandLine);
	else if (commandLineOption == SHOW_REPLY)
		showReply(commandLine);
	else if (commandLineOption == JEEK)
		jeek(commandLine);
	else if (commandLineOption == LIKE)
		like(commandLine);
	else if (commandLineOption == DISLIKE)
		dislike(commandLine);
	else if (commandLineOption == FOLLOW)
		follow(commandLine);
	else if (commandLineOption == UNFOLLOW)
		unfollow(commandLine);
	else if (commandLineOption == NOTIFICATIONS)
		notifications(commandLine);
	else
		throw BadOperationException();
}

void JeekJeek::signup(string commandLine)
{
	string username = findWhiteSpaces(commandLine);
	commandLine.erase(0,username.size());
	string displayName = findWhiteSpaces(commandLine);
	commandLine.erase(0,displayName.size());
	string password = findWhiteSpaces(commandLine);
	commandLine.erase(0,password.size());

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (findUser(username) != NOT_AN_USER)
		throw UserDuplicateException();
	try
	{
		users.push_back(new User(username, displayName, password));
		cout << SIGNUP_SUCCESSFUL << endl;
	}
	catch(UserSignupException badException)
	{
		cout << SIGNUP_FAILED << endl;
	}
}

void JeekJeek::login(string commandLine)
{
	string username = findWhiteSpaces(commandLine);
	commandLine.erase(0,username.size());
	string password = findWhiteSpaces(commandLine);
	commandLine.erase(0,password.size());
	int loginUserNum = findUser(username);

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (onlineUserNum != NOT_AN_USER)
		throw UserLoginException1();
	else if (loginUserNum == NOT_AN_USER)
		throw UserLoginException2();
	else if(users[loginUserNum]->passwordIsCorrect(password) == false)
		throw UserLoginException2();
	
	onlineUserNum = loginUserNum;
	cout << HELLO << users[onlineUserNum]->getDisplayName() << EXCLAMATION_MARK << endl;
}

void JeekJeek::logout(string commandLine)
{
	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();
	
	cout << BYEBYE << users[onlineUserNum]->getDisplayName() << EXCLAMATION_MARK << endl;
	onlineUserNum = NOT_AN_USER;
}

void JeekJeek::jeek(string commandLine)
{
	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();
	try
	{
		jeeks.push_back(new Jeek());
		jeeks.back()->setJeekBaseInfo(generateJeekId(), users[onlineUserNum]->getDisplayName(), users[onlineUserNum]->getUserName());
		users[onlineUserNum]->setJeekForUser(jeeks.back());
		cout << JEEK_PUBLISHED << endl;
		users[onlineUserNum]->notifForFollowers(jeeks.back()->getJeekId());
		addNotifForMentionedUsers();
	}
	catch(AbortedJeek abortedJeek)
	{
		cout << JEEK_ABORTED << endl;
	}
}

string JeekJeek::generateJeekId()
{
	string newId = JEEK_ID;
	srand(time(NULL));
	int newIdNum = rand();
	newId.append(intToString(newIdNum));
	return newId;
}

void JeekJeek::comment(string commandLine)
{
	string commentingJeekId = findWhiteSpaces(commandLine);
	commandLine.erase(0,commentingJeekId.size());
	int commentingJeekNum = findJeek(commentingJeekId);

	findWhiteSpaces(commandLine);
	string commentText = commandLine;

	if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();
	else if (commentText == NOTHING)
		throw TextException();
	else if (commentingJeekNum == NOT_A_JEEK)
		throw JeekFindingException();

	Comment* newComment = jeeks[commentingJeekNum]->addComment(commentText, users[onlineUserNum]->getDisplayName(), users[onlineUserNum]->getUserName());
	users[onlineUserNum]->setCommentForUser(newComment);
	cout << COMMENT_ADDED << endl;
	int commentedUserNum = findUser(jeeks[commentingJeekNum]->getJeekerUsername());
	users[commentedUserNum]->addNotifForComment(users[onlineUserNum]->getUserName(), commentingJeekId);
}

void JeekJeek::reply(string commandLine)
{
	string replyingThingId = findWhiteSpaces(commandLine);
	commandLine.erase(0,replyingThingId.size());
	findWhiteSpaces(commandLine);
	string replyText = commandLine;

	if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();
	else if (replyText == NOTHING)
		throw TextException();
	else if (replyingThingId.find(REPLY_ID) > replyingThingId.size())
		replyForComment(replyingThingId, replyText);
	else
		replyForReply(replyingThingId, replyText);
	
	cout << REPLY_ADDED << endl;
}

void JeekJeek::replyForComment(string commentId, string replyText)
{
	string jeekId = divideString(commentId, 0, commentId.find(COMMENT_ID));
    int jeekNum = findJeek(jeekId);
       
    if (jeekNum == NOT_A_JEEK)
    	throw CommentFindingException();

    Reply* newReply = jeeks[jeekNum]->addReplyForComment(replyText, commentId, users[onlineUserNum]->getDisplayName(), users[onlineUserNum]->getUserName());
    users[onlineUserNum]->setReplyForUser(newReply);
    int repliedUserNum = findUser(jeeks[jeekNum]->findUserOfComment(commentId));
	users[repliedUserNum]->addNotifForReply(users[onlineUserNum]->getUserName(), commentId);
}

void JeekJeek::replyForReply(string replyId, string replyText)
{
    string jeekId = divideString(replyId, 0, replyId.find(COMMENT_ID));
    int jeekNum = findJeek(jeekId);
       
    if (jeekNum == NOT_A_JEEK)
    	throw ReplyFindingException();

    Reply* newReply = jeeks[jeekNum]->addReplyForReply(replyText, replyId, users[onlineUserNum]->getDisplayName(), users[onlineUserNum]->getUserName());
    users[onlineUserNum]->setReplyForUser(newReply);
	int repliedUserNum = findUser(jeeks[jeekNum]->findUserOfReply(replyId));
	users[repliedUserNum]->addNotifForReply(users[onlineUserNum]->getUserName(), replyId);
}

void JeekJeek::rejeek(string commandLine)
{
	string jeekId = findWhiteSpaces(commandLine);
	commandLine.erase(0,jeekId.size());
	int jeekNum = findJeek(jeekId);

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();
	else if (jeekNum == NOT_A_JEEK)
		throw JeekFindingException();

	Jeek* newJeek = jeeks[jeekNum]->rejeek();
	jeeks.push_back(newJeek);
	jeeks.back()->setJeekBaseInfo(generateJeekId(), users[onlineUserNum]->getDisplayName(), users[onlineUserNum]->getUserName());
	users[onlineUserNum]->setJeekForUser(jeeks.back());
	cout << JEEK_REJEEKED << endl;
	int rejeekedUserNum = findUser(jeeks[jeekNum]->getJeekerUsername());
	users[rejeekedUserNum]->addNotifForRejeek(users[onlineUserNum]->getUserName(), jeekId);
}

void JeekJeek::search(string commandLine)
{
	string searchingValue = findWhiteSpaces(commandLine);
	commandLine.erase(0,searchingValue.size());

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (searchingValue[0] == ATSIGN)
		searchWithUseranme(divideString(searchingValue, 1, searchingValue.size()));
	else if (searchingValue[0] == HASHTAG)
		searchWithHashtag(divideString(searchingValue, 1, searchingValue.size()));
	else
		throw BadOperationException();
}

void JeekJeek::searchWithUseranme(string username)
{
	int userNum = findUser(username);

	if (userNum == NOT_AN_USER)
		throw JeekFindingException();

	users[userNum]->showJeek();
}

void JeekJeek::searchWithHashtag(string hashtagString)
{
	bool anyJeekFound = false;
	for (int i = 0; i < jeeks.size(); i++)
	{
		bool jeekShowed = jeeks[i]->showJeekForHashtag(hashtagString);
		if (jeekShowed == true)
			anyJeekFound = true;
	}

	if (anyJeekFound == false)
		throw JeekFindingException();
}

void JeekJeek::showJeek(string commandLine)
{
	string jeekId = findWhiteSpaces(commandLine);
	commandLine.erase(0,jeekId.size());
	int jeekNum = findJeek(jeekId);

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (jeekNum == NOT_A_JEEK)
		throw JeekFindingException();

	jeeks[jeekNum]->showCompeleteJeek();
}

void JeekJeek::showComment(string commandLine)
{
	string commentId = findWhiteSpaces(commandLine);
	commandLine.erase(0,commentId.size());
	string jeekId = divideString(commentId, 0, commentId.find(COMMENT_ID));
    int jeekNum = findJeek(jeekId);

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (jeekNum == NOT_A_JEEK)
		throw CommentFindingException();

	jeeks[jeekNum]->showComment(commentId);
}

void JeekJeek::showReply(string commandLine)
{
	string replyId = findWhiteSpaces(commandLine);
	commandLine.erase(0,replyId.size());
	string jeekId = divideString(replyId, 0, replyId.find(COMMENT_ID));
	int jeekNum = findJeek(jeekId);

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (jeekNum == NOT_A_JEEK)
		throw ReplyFindingException();

	jeeks[jeekNum]->showReply(replyId);
}

void JeekJeek::like(string commandLine)
{
	string jeekId = findWhiteSpaces(commandLine);
	commandLine.erase(0,jeekId.size());
	int jeekNum = findJeek(jeekId);

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();
	else if (jeekNum == NOT_A_JEEK)
		throw JeekFindingException();
	try
	{
		jeeks[jeekNum]->like(users[onlineUserNum]->getUserName());
		int likedUserNum = findUser(jeeks[jeekNum]->getJeekerUsername());
		cout << JEEK_LIKED << endl;
		users[likedUserNum]->addNotifForLike(users[onlineUserNum]->getUserName(), jeekId);
	}
	catch(JeekLikeException likedBefore)
	{
		cout << LIKED_BEFORE << endl;
	}
}

void JeekJeek::dislike(string commandLine)
{
	string jeekId = findWhiteSpaces(commandLine);
	commandLine.erase(0,jeekId.size());
	int jeekNum = findJeek(jeekId);

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();
	else if (jeekNum == NOT_A_JEEK)
		throw JeekFindingException();
	try
	{
		jeeks[jeekNum]->dislike(users[onlineUserNum]->getUserName());
		int dislikedUserNum = findUser(jeeks[jeekNum]->getJeekerUsername());
		cout << JEEK_DISLIKED << endl;
		users[dislikedUserNum]->addNotifForDislike(users[onlineUserNum]->getUserName(), jeekId);
	}
	catch(JeekDislikeException notLikedBefore)
	{
		cout << NOT_LIKED_BEFORE << endl;		
	}
}

void JeekJeek::follow(string commandLine)
{
	string username = findWhiteSpaces(commandLine);
	commandLine.erase(0,username.size());
	int userNum = findUser(username);

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();
	else if (userNum == NOT_AN_USER)
		throw UserFindingException();
	try
	{
		users[onlineUserNum]->follow(users[userNum]);
		cout << USER_FOLLOWED << endl;
		users[userNum]->setFollower(users[onlineUserNum]);
	}
	catch(UserFollowException followedBefore)
	{
		cout << FOLLOWED_BEFORE << endl;
	}
}

void JeekJeek::unfollow(string commandLine)
{
	string username = findWhiteSpaces(commandLine);
	commandLine.erase(0,username.size());
	int userNum = findUser(username);

	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();
	else if (userNum == NOT_AN_USER)
		throw UserFindingException();
	try
	{
		users[onlineUserNum]->unfollow(users[userNum]);
		cout << USER_UNFOLLOWED << endl;
		users[userNum]->unsetFollower(users[onlineUserNum]);
	}
	catch(UserUnfollowException notFollowedBefore)
	{
		cout << NOT_FOLLOWED_BEFORE << endl;
	}
}

void JeekJeek::addNotifForMentionedUsers()
{
	string jeekId = jeeks.back()->getJeekId();
    std::vector<std::string> mentionedUsernames = jeeks.back()->getJeekMentions();

   	for (int i = 0; i < mentionedUsernames.size(); i++)
   	{
   		int mentionedUserNum = findUser(mentionedUsernames[i]);
   		if (mentionedUserNum != NOT_AN_USER)
   			users[mentionedUserNum]->addNotifForMention(users[onlineUserNum]->getUserName(), jeekId);
   	}
}

void JeekJeek::notifications(string commandLine)
{
	if (findWhiteSpaces(commandLine) != NOTHING)
		throw BadOperationException();
	else if (onlineUserNum == NOT_AN_USER)
		throw UserLogoutException();

	users[onlineUserNum]->showNotifications();
}

int JeekJeek::findUser(string username)
{
	for (int i = 0; i < users.size(); i++)
		if (users[i]->getUserName() == username)
			return i;
	return NOT_AN_USER;
}

int JeekJeek::findJeek(string jeekId)
{
	for (int i = 0; i < jeeks.size(); i++)
		if (jeeks[i]->getJeekId() == jeekId)
			return i;
	return NOT_A_JEEK;
}