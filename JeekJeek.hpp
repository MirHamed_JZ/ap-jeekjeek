#ifndef __JEEKJEEK_H__
#define __JEEKJEEK_H__

#include "user.hpp"

#define NOT_AN_USER -1
#define NOT_A_JEEK -2
#define EXCLAMATION_MARK '!'
#define HELLO "Hi "
#define BYEBYE "Bye "
#define SIGNUP "signup"
#define LOGIN "login"
#define LOGOUT "logout"
#define SEARCH "search"
#define JEEK "jeek"
#define REJEEK "rejeek"
#define COMMENT "comment"
#define REPLY "reply"
#define SHOW_JEEK "showJeek"
#define SHOW_COMMENT "showComment"
#define SHOW_REPLY "showReply"
#define LIKE "like"
#define DISLIKE "dislike"
#define FOLLOW "follow"
#define UNFOLLOW "unfollow"
#define NOTIFICATIONS "notifications"
#define TEXT_EMPTY "Text is empty."

class JeekJeek
{
  public:
    JeekJeek() {}
    void commandLineOperation(std::string commandLine);
    void anyBodyIsOnline() { onlineUserNum = NOT_AN_USER; }
  private:
    std::vector<User*> users;
    std::vector<Jeek*> jeeks;
    int onlineUserNum;

    void doOperation(std::string commandLine, std::string commandLineOption);
    void signup(std::string commandLine);
    void login(std::string commandLine);
    void logout(std::string commandLine);
    void jeek(std::string commandLine);
    void comment(std::string commandLine);
    void reply(std::string commandLine);
    void replyForComment(std::string commentId, std::string replyText);
    void replyForReply(std::string replyId, std::string replyText);
    void rejeek(std::string commandLine);
    void search(std::string commandLine);
    void searchWithUseranme(std::string username);
    void searchWithHashtag(std::string hashtagString);
    void showJeek(std::string commandLine);
    void showComment(std::string commandLine);
    void showReply(std::string commandLine);
    void like(std::string commandLine);
    void dislike(std::string commandLine);
    void follow(std::string commandLine);
    void unfollow(std::string commandLine);
    void addNotifForMentionedUsers();
    void notifications(std::string commandLine);
    std::string generateJeekId();
    int findUser(std::string username);
    int findJeek(std::string jeekId);
};

#endif