#include <iostream>
#include "comment.hpp"

using namespace std;

Comment::Comment(string _commentText, string _commenterDisplayName)
{
	commentText = _commentText;
	commenterDisplayName = _commenterDisplayName;
}

void Comment::setCommentBaseInfo(string _commentId, string _commenterUsername)
{
	commentId = _commentId;
	commenterUsername = _commenterUsername;
}

Reply* Comment::addReplyForComment(string replyText, string displayName, string username)
{
	commentReplies.push_back(new Reply(replyText, displayName));
	commentReplies.back()->setReplyBaseInfo(generateIdForReplyOfComment(), username);
	return commentReplies.back();
}

Reply* Comment::addReplyForReply(string replyText, string replyId, string displayName, string username)
{
	int replyNum = findReply(replyId);

	if (replyNum == NOT_A_REPLY)
    	throw ReplyFindingException();

    return commentReplies[replyNum]->addReply(replyText, displayName, username);
}

void Comment::showComment()
{
	cout << commenterDisplayName << endl << commentText << endl;
	cout << REPLIES_TITLE << endl;
	for (int i = 0; i < commentReplies.size(); i++)
		cout << commentReplies[i]->getReplyId() << endl;
}

void Comment::showReply(string replyId)
{
	int replyNum = findReply(replyId);

	if (replyNum == NOT_A_REPLY)
    	throw ReplyFindingException();

    commentReplies[replyNum]->showReply();
}

string Comment::generateIdForReplyOfComment()
{
	string newId = commentId + REPLY_ID;
	srand(time(NULL));
	int newIdNum = rand();
	newId.append(intToString(newIdNum));
	return newId;
}

int Comment::findReply(string replyId)
{
	for (int i = 0; i < commentReplies.size(); i++)
		if (commentReplies[i]->getReplyId() == replyId)
			return i;
	return NOT_A_REPLY;
}

string Comment::findUserOfReply(string replyId)
{
	for (int i = 0; i < commentReplies.size(); i++)
		if (commentReplies[i]->getReplyId() == replyId)
			return commentReplies[i]->getReplierUsername();
	return NOTHING;
}