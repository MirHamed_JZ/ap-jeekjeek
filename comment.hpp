#ifndef __COMMENT_H__
#define __COMMENT_H__

#include "reply.hpp"

#define NOT_A_COMMENT -3
#define COMMENTS_TITLE "Comments:"
#define COMMENT_ADDED "Your comment added successfully."
#define COMMENT_ID "_C_"
#define COMMENT_NOT_FOUND "There is no comment with this information."

class CommentFindingException {};

class Comment
{
  public:
    Comment(std::string _commentText, std::string _commenterDisplayName);
    Reply* addReplyForComment(std::string replyText, std::string displayName, std::string username);
    Reply* addReplyForReply(std::string replyText, std::string replyId, std::string displayName, std::string username);
    void showComment();
    void showReply(std::string replyId);
    void setCommentBaseInfo(std::string _commentId, std::string _commenterUsername);
    std::string getCommenterUsername() { return commenterUsername; }
    std::string getCommentId() { return commentId; }
    std::string findUserOfReply(std::string replyId);
  private:
    std::string commenterDisplayName;
    std::string commenterUsername;
    std::string commentId;
    std::string commentText;
    std::vector<Reply*> commentReplies;

    std::string generateIdForReplyOfComment();
    int findReply(std::string replyId);
};

#endif