#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include "JeekJeek.hpp"

class CommandLineInterface
{
  public:
    CommandLineInterface(){ jeekJeek.anyBodyIsOnline(); };
    void commandLineOperation(std::string commandLine);
  private:
    JeekJeek jeekJeek;
};

#endif