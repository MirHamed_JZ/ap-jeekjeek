#include <iostream>
#include "interface.hpp"

using namespace std;

int main()
{
	CommandLineInterface commandLineInterface;
	string commandLine;
	while(getline(cin,commandLine))
		commandLineInterface.commandLineOperation(commandLine);
}