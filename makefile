a.out: main.o interface.o JeekJeek.o user.o Jeek.o comment.o reply.o
	g++ main.o interface.o JeekJeek.o user.o Jeek.o comment.o reply.o
main.o: main.cpp interface.o
	g++ -c main.cpp
interface.o: interface.cpp interface.hpp JeekJeek.o
	g++ -c interface.cpp
JeekJeek.o: JeekJeek.cpp JeekJeek.hpp user.o
	g++ -c JeekJeek.cpp
user.o: user.cpp user.hpp Jeek.o
	g++ -c user.cpp
Jeek.o: Jeek.cpp Jeek.hpp comment.o
	g++ -c Jeek.cpp
comment.o: comment.cpp comment.hpp reply.o
	g++ -c comment.cpp
reply.o: reply.cpp reply.hpp
	g++ -c reply.cpp
clean:
	rm *.o
	rm a.out