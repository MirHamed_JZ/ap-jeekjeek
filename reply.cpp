#include <iostream>
#include "reply.hpp"

using namespace std;

Reply::Reply(string _replyText, std::string _replierDisplayName)
{
	replyText = _replyText;
	replierDisplayName = _replierDisplayName;
}

void Reply::setReplyBaseInfo(string _replyId, string _replierUsername)
{
	replyId = _replyId;
	replierUsername = _replierUsername;
}

Reply* Reply::addReply(string replyText, string displayName, string username)
{
	replies.push_back(new Reply(replyText, displayName));
	replies.back()->setReplyBaseInfo(generateIdForReplyOfReply(), username);
	return replies.back();
}

void Reply::showReply()
{
	cout << replierDisplayName << endl << replyText << endl;
	cout << REPLIES_TITLE << endl;
	for (int i = 0; i < replies.size(); i++)
		cout << replies[i]->replyId << endl;
}

string Reply::generateIdForReplyOfReply()
{
	string newId = replyId + REPLY_ID;
	srand(time(NULL));
	int newIdNum = rand();
	newId.append(intToString(newIdNum));
	return newId;
}

string intToString(int intNum)
{
	string stringNum = NOTHING;
	vector<int> intNumDigits;
	if (intNum == 0)
		stringNum = ZERO_CHAR;
	while (intNum != 0)
	{
		intNumDigits.push_back(intNum % 10);
		intNum /= 10;
	}
	for (int i = intNumDigits.size() - 1; i >= 0; i--)
		stringNum += (char)(intNumDigits[i] + ZERO_CHAR);
	return stringNum;
}