#ifndef __REPLY_H__
#define __REPLY_H__

#include <ctime>
#include <cstdlib>
#include <string>
#include <vector>

#define NOT_A_REPLY -4
#define NOTHING ""
#define ZERO_CHAR '0'
#define REPLY_ADDED "Your reply added successfully."
#define REPLY_ID "_R_"
#define REPLIES_TITLE "Replies:"
#define REPLY_NOT_FOUND "There is no reply with this information."

std::string intToString(int intNum);

class TextException {};
class ReplyFindingException {};

class Reply
{
  public:
    Reply(std::string _replyText, std::string _replierDisplayName);
    Reply* addReply(std::string replyText, std::string displayName, std::string username);
    void showReply();
    void setReplyBaseInfo(std::string _replyId, std::string _replierUsername);
    std::string getReplierUsername() { return replierUsername; }
    std::string getReplyId() { return replyId; }
  private:
    std::string replierDisplayName;
    std::string replierUsername;
    std::string replyId;
    std::string replyText;
    std::vector<Reply*> replies;

    std::string generateIdForReplyOfReply();
};

#endif