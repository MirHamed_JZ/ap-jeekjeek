#include <iostream>
#include "user.hpp"

using namespace std;

User::User(string _username, string _displayName, string _password)
{
	if ((_username == NOTHING) || (_displayName == NOTHING) || (_password == NOTHING))
		throw UserSignupException();
	username = _username;
	displayName = _displayName;
	password = _password;
}

void User::showJeek()
{
	if (userJeeks.size() == 0)
		cout << NO_JEEK_FOR_USER << endl;
	else
	{
		for (int i = 0; i < userJeeks.size(); i++)
		{
			cout << userJeeks[i]->getJeekId() << SPACE << displayName << endl;
			cout << userJeeks[i]->getJeekText() << endl << endl;
		}
	}
}

bool User::passwordIsCorrect(string _password)
{
	if (password == _password)
		return true;
	return false;
}

void User::follow(User* user)
{
	if (isUserFollowedBefore(user) == true)
		throw UserFollowException();

	followings.push_back(user);
}

void User::setFollower(User* user)
{
	followers.push_back(user);
}

void User::unfollow(User* user)
{
	if (isUserFollowedBefore(user) == false)
		throw UserUnfollowException();

	deleteFollowingFromList(user);
}

void User::unsetFollower(User* user)
{
	deleteFollowerFromList(user);
}

bool User::isUserFollowedBefore(User* user)
{
	for (int i = 0; i < followings.size(); i++)
		if (followings[i] == user)
			return true;
	return false;
}

void User::deleteFollowingFromList(User* user)
{
	for (int i = 0; i < followings.size(); i++)
	{
		if (followings[i] == user)
		{
			for (int j = i; j < followings.size() - 1; j++)
				followings[j] = followings [j + 1];
			followings.pop_back();
			break;
		}
	}
}

void User::deleteFollowerFromList(User* user)
{
	for (int i = 0; i < followers.size(); i++)
	{
		if (followers[i] == user)
		{
			for (int j = i; j < followers.size() - 1; j++)
				followers[j] = followers [j + 1];
			followers.pop_back();
			break;
		}
	}
}

void User::addNotifForLike(string likerUsername, string jeekId)
{
	string notif = likerUsername + LIKED + jeekId;
	notifications.push_back(notif);
}

void User::addNotifForDislike(string dislikerUsername, string jeekId)
{
	string notif = dislikerUsername + DISLIKED + jeekId;
	notifications.push_back(notif);
}

void User::addNotifForComment(string commenterUsername, string jeekId)
{
	string notif = commenterUsername + COMMENTED + jeekId;
	notifications.push_back(notif);
}

void User::addNotifForReply(string replierUsername, string replyingThingId)
{
	string notif = replierUsername + REPLIED + replyingThingId;
	notifications.push_back(notif);
}

void User::notifForFollowers(string jeekId)
{
	for (int i = 0; i < followers.size(); i++)
		followers[i]->addNotifForJeekOfFollowing(username, jeekId);
}

void User::addNotifForJeekOfFollowing(string jeekerusername, string jeekId)
{
	string notif = jeekerusername + JEEKED + jeekId;
	notifications.push_back(notif);
}

void User::addNotifForMention(string mentionerUsername, string jeekId)
{
	string notif = mentionerUsername + MENTIONED + jeekId;
	notifications.push_back(notif);
}

void User::addNotifForRejeek(string rejeekerUsername, string jeekId)
{
	string notif = rejeekerUsername + REJEEKED + jeekId;
	notifications.push_back(notif);
}

void User::showNotifications()
{
	if (notifications.size() == 0)
		cout << NO_NOTIFICATION << endl;
	else
	{
		for (int i = 0; i < notifications.size(); i++)
			cout << notifications[i] << endl;
		deleteNotifications();
	}
}

void User::deleteNotifications()
{
	int notifsSize = notifications.size();
	for (int i = 0; i < notifsSize; i++)
		notifications.pop_back();
}

void User::setJeekForUser(Jeek* addingJeek)
{
	userJeeks.push_back(addingJeek);
}

void User::setCommentForUser(Comment* addingComment)
{
	userComments.push_back(addingComment);
}

void User::setReplyForUser(Reply* addingReply)
{
	userReplies.push_back(addingReply);
}