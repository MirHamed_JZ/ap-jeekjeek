#ifndef __USER_H__
#define __USER_H__

#include "Jeek.hpp"

#define SIGNUP_SUCCESSFUL "Signup was successful."
#define SIGNUP_FAILED "Signup failed."
#define USER_DUPLICATE "Username is repetitious."
#define LOGIN_FAILED "username or password is incorrect."
#define USER_IS_ONLINE "An user is online."
#define NO_USER_ONLINE "There is no user online."
#define USER_NOT_FOUND "This user doesn't exists."
#define NO_JEEK_FOR_USER "This user has no Jeek."
#define NO_NOTIFICATION "You have not any notifications."
#define USER_FOLLOWED "Followed successfully."
#define USER_UNFOLLOWED "Unfollowed successfully."
#define FOLLOWED_BEFORE "You followed this user before."
#define NOT_FOLLOWED_BEFORE "You didn't follow this user before."
#define LIKED " liked "
#define DISLIKED " disliked "
#define COMMENTED " commented on "
#define REPLIED " replied "
#define REJEEKED " rejeeked "
#define JEEKED " jeeked "
#define MENTIONED " mentioned you in "

class UserSignupException {};
class UserDuplicateException {};
class UserLoginException1 {};
class UserLoginException2 {};
class UserLogoutException {};
class UserFindingException {};
class UserFollowException {};
class UserUnfollowException {};

class User
{
  public:
    User(std::string _username, std::string _displayName, std::string _password);
    void showJeek();
    bool passwordIsCorrect(std::string _password);
    void follow(User* user);
    void setFollower(User* user);
    void unfollow(User* user);
    void unsetFollower(User* user);
    void addNotifForLike(std::string likerUsername, std::string jeekId);
    void addNotifForDislike(std::string dislikerUsername, std::string jeekId);
    void addNotifForComment(std::string commenterUsername, std::string jeekId);
    void addNotifForRejeek(std::string rejeekerUsername, std::string jeekId);
    void addNotifForReply(std::string replierUsername, std::string replyingThingId);
    void notifForFollowers(std::string jeekId);
    void addNotifForMention(std::string mentionerUsername, std::string jeekId);
    void showNotifications();
    void setJeekForUser(Jeek* addingJeek);
    void setCommentForUser(Comment* addingComment);
    void setReplyForUser(Reply* addingReply);
    std::string getUserName() { return username; }
    std::string getDisplayName() { return displayName; }
  private:
  	std::string username;
  	std::string displayName;
  	std::string password;
  	std::vector<Jeek*> userJeeks;
  	std::vector<Comment*> userComments;
  	std::vector<Reply*> userReplies;
  	std::vector<User*> followers;
  	std::vector<User*> followings;
  	std::vector<std::string> notifications;

	void addNotifForJeekOfFollowing(std::string jeekerusername, std::string jeekId);
  	bool isUserFollowedBefore(User* user);
  	void deleteFollowingFromList(User* user);
  	void deleteFollowerFromList(User* user);
    void deleteNotifications();
};

#endif